<?php

namespace App\Repository;

use App\Entity\Version;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VersionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Version::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('v')
            ->where('v.something = :value')->setParameter('value', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findCurrent($fileId)
    {
        $query = $this->createQueryBuilder('v')
            ->where('v.file_id = :fileId')->setParameter('fileId', $fileId)
            ->orderBy('v.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        try{
            return
                $query->getSingleResult();
        } catch(\Exception  $e) {
            return null;
        }
    }
    */
}
