<?php

namespace App\Repository;

use App\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function getFilteredFiles($filters)
    {
        $request = $this->createQueryBuilder('f');

        if(isset($filters['keywords'])):
            $keywords = explode('|', $filters['keywords']);
            $this->addKeywordsFilter($request, $keywords);
        endif;

        if(isset($filters['type'])):
            $this->addTypeFilter($request, $filters['type']);
        endif;

        $request
            ->orderBy('f.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    function addKeywordsFilter($request, $keywords)
    {
        $request->join('f.keywords', 'k')
            ->where('k.keyword IN (:keywords)')
            ->setParameter('keywords', $keywords);
    }

    function addTypeFilter($request, $type)
    {
        $request->join('f.versions', 'v')
            ->join('v.type_mime', 't')
            ->where('t.idTypeDocument = (:type)')
            ->setParameter('type', $type);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('f')
            ->where('f.something = :value')->setParameter('value', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
