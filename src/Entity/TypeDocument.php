<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeDocumentRepository")
 * @ORM\Table(name="type_document")
 */
class TypeDocument
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TypeMime", mappedBy="typeDocument")
     */
    private $typesMime;

    /**
     * TypeDocument constructor.
     */
    public function __construct()
    {
        $this->typesMime = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of label
     */ 
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of label
     *
     * @return  self
     */ 
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypesMime()
    {
        return $this->typesMime;
    }

    /**
     * @param mixed $typesMime
     * @return self
     */
    public function setTypesMime(ArrayCollection $typesMime)
    {
        $this->typesMime = $typesMime;
        return $this;
    }

    /**
     * @param TypeMime $typeMime
     * @return self
     */
    public function addTypeMime(TypeMime $typeMime)
    {
        $this->typesMime->add($typeMime);
        $typeMime->setTypeDocument($this);
        return $this;
    }

    /**
     * @param TypeMime $typeMime
     * @return self
     */
    public function removeTypeMime(TypeMime $typeMime)
    {
        $this->typesMime->removeElement($typeMime);
        $typeMime->setTypeDocument(null);
        return $this;
    }
}
