<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="sso_id", type="integer")
     */
    private $ssoId;

    /**
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Version", mappedBy="user")
     */
    private $versions;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of ssoId
     */
    public function getSsoId()
    {
        return $this->ssoId;
    }

    /**
     * Set the value of ssoId
     *
     * @return  self
     */
    public function setSsoId($ssoId)
    {
        $this->ssoId = $ssoId;
        return $this;
    }

    /**
     * Get the value of name
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Get the value of lastName
     */ 
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName
     *
     * @return  self
     */ 
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param ArrayCollection $versions
     * @return self
     */
    public function setVersions(ArrayCollection $versions)
    {
        $this->versions = $versions;
        return $this;
    }

    /**
     * @param Version $version
     * @return self
     */
    public function addVersion(Version $version)
    {
        $this->versions->add($version);
        return $this;
    }

    /**
     * @param Version $version
     * @return self
     */
    public function removeVersion(Version $version)
    {
        $this->versions->removeElement($version);
        return $this;
    }
}
