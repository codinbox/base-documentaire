<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VersionRepository")
 * @ORM\Table(name="version")
 */
class Version
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="version", type="integer")
     */
    private $version = 1;

    /**
     * @ORM\Column(name="path", type="text")
     */
    private $path;

    /**
     * @ORM\Column(name="deposit_date", type="date")
     */
    private $depositDate;

    /**
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\File", inversedBy="versions")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $file = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="versions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeMime", inversedBy="versions")
     * @ORM\JoinColumn(name="type_mime_id", referencedColumnName="id")
     */
    private $typeMime = null;

    /**
     * Version constructor.
     */
    public function __construct()
    {
        $this->depositDate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of version
     */ 
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set the value of version
     *
     * @return  self
     */ 
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Get the value of path
     */ 
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     *
     * @return  self
     */ 
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get the value of deposit_date
     */ 
    public function getDepositDate()
    {
        return $this->depositDate;
    }

    /**
     * Set the value of deposit_date
     *
     * @return  self
     */ 
    public function setDepositDate($depositDate)
    {
        $this->depositDate = $depositDate;

        return $this;
    }

    /**
     * Get the value of file
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @return  self
     */ 
    public function setFile($file)
    {
        $this->file = $file;
		$file->setLastVersion($this);
		$file->addVersion($this);
        return $this;
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;
		$user->addVersion($this);
        return $this;
    }

    /**
     * Get the value of typeMime
     */
    public function getTypeMime()
    {
        return $this->typeMime;
    }

    /**
     * Set the value of typeMime
     *
     * @return  self
     */
    public function setTypeMime($typeMime)
    {
        $this->typeMime = $typeMime;
        return $this;
    }

    /**
     * Get the value of size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }
}
