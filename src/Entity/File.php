<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 * @ORM\Table(name="file")
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(name="pin", type="integer", nullable=true)
     */
    private $pin;

    /**
     * One File has one LastVersion
     * @ORM\OneToOne(targetEntity="App\Entity\Version", cascade={"persist"}, orphanRemoval= true)
     * @ORM\JoinColumn(name="last_version_id", referencedColumnName="id", nullable=true, onDelete="set null")
     */
    private $lastVersion = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Version", mappedBy="file")
     */
    private $versions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keyword", mappedBy="files")
     */
    private $keywords;

    public function __construct()
    {

        $this->versions = new ArrayCollection();
        $this->keywords = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get the value of pin
     */ 
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set the value of pin
     *
     * @return  self
     */ 
    public function setPin($pin)
    {
        $this->pin = $pin;
        return $this;
    }

    /**
     * @return Version
     */
    public function getLastVersion()
    {
        return $this->lastVersion;
    }

    /**
     * @param Version $lastVersion
     * @return self
     */
    public function setLastVersion(Version $lastVersion)
    {
        $this->lastVersion = $lastVersion;
        return $this;
    }

    /**
     * Get the value of versions
     */ 
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * Set the value of versions
     *
     * @return  self
     */ 
    public function addVersion($version)
    {
        $this->versions[] = $version;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param ArrayCollection $keywords
     * @return mixed
     */
    public function setKeywords(ArrayCollection $keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    /**
     * @param Keyword $keyword
     * @return self
     */
    public function addKeyword(Keyword $keyword)
    {
        $this->keywords->add($keyword);
        $keyword->addFile($this);
        return $this;
    }

    /**
     * @param File $file
     * @return self
     */
    public function removeKeyword(File $file)
    {
        $this->files->removeElement($file);
        return $this;
    }

    /**
     * @return boolean
     */
	function hasPin()
	{
		$hasPin = true;
		if(null === $this->pin):
			$hasPin = false;
		endif;
		return $hasPin;
	}
}
