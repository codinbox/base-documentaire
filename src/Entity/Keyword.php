<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KeywordRepository")
 * @ORM\Table(name="keyword")
 */
class Keyword
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="keyword", type="string", length=24)
     */
    private $keyword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="keywords")
     * @ORM\JoinTable(name="keyword_file")
     */
    private $files;

    /**
     * Keyword constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of keyword
     */ 
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set the value of keyword
     *
     * @return  self
     */ 
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param File $file
     * @return self
     */
    public function addFile(File $file)
    {
        $this->files->add($file);
        return $this;
    }

    /**
     * @param File $file
     * @return self
     */
    public function removeFile(File $file)
    {
        $this->file->removeElement($file);
        return $this;
    }
}
