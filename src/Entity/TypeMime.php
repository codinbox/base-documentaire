<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeMimeRepository")
 * @ORM\Table(name="type_mime")
 */
class TypeMime
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="mime", type="string", length=255)
     */
    private $mime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeDocument", inversedBy="typesMime")
     * @ORM\JoinColumn(name="type_document_id", referencedColumnName="id")
     */
    private $typeDocument = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Version", mappedBy="typeMime")
     */
    private $versions;

    /**
     * Keyword constructor.
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the value of mime
     */ 
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Set the value of mime
     *
     * @param $mime
     * @return  self
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
        return $this;
    }

    /**
     * @return TypeDocument $typeDocument
     */
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }

    /**
     * @param TypeDocument $typeDocument
     * @return self
     */
    public function setTypeDocument($typeDocument)
    {
        $this->typeDocument = $typeDocument;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param ArrayCollection $versions
     * @return self
     */
    public function setVersions(ArrayCollection $versions )
    {
        $this->versions = $versions;
        return $this;
    }

    /**
     * @param Version $version
     * @return self
     */
    public function addVersion(Version $version )
    {
        $this->versions->add($version);
        $version->setTypeMime($this);
        return $this;
    }

    /**
     * @param Version $version
     * @return self
     */
    public function removeVersion(Version $version)
    {
        $this->versions->removeElement($version);
        $version->setTypeMime(null);
        return $this;
    }
}
