<?php

namespace App\Controller;

use App\Entity\Keyword;
use App\Entity\TypeDocument;
use App\Entity\TypeMime;
use App\Entity\File;
use App\Entity\Version;
use App\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Unirest;

/**
 * @Route("/api/{token}")
 */
class ApiWithTokenController extends Controller implements TokenAuthenticatedController
{
	/**
	 * Fonction d'upload de version
	 */
	function uploadFile($file, $user, $upload) {
        $manager = $this->getDoctrine()->getManager();
		
		// on génére un nom de fichier
		$extension = $upload->getClientOriginalExtension();
		$filename = uniqid().".".$extension;
		
		// récupération du nom de répertoire défini dans les paramétres
		// sauvegarde du fichier dans le répertoire
		$path = $this->getParameter('files');
		$upload->move($path,$filename);

		// on vérifie s'il existe déjà un type mime corrspondant en base, sinon on le créé
		$typeMimeRepository = $manager->getRepository(TypeMime::class);
		$typeMime = $typeMimeRepository->findOneBy(array('mime'=> $extension));
		if ($typeMime === null) {
			$typeMime = new TypeMime();
			$typeMime->setMime($extension);
			$manager->persist($typeMime);
			$manager->flush();
		}
		$fileSize =  $upload->getClientSize();

		// création du fichier versionné
		$version = new Version();
		$version
			->setFile($file)
			->setUser($user)
			->setPath($path.$filename)
			->setDepositDate(new \DateTime())
			->setSize($fileSize)
			->setTypeMime($typeMime);
		if($file->getLastVersion()):
			$version->setVersion($file->getLastVersion()->getVersion() + 1);
		endif;
		$manager->persist($version);
		$manager->flush();
		return $version;
	}
	
    /**
     * Téléchargement des fichiers
     *
     * @Route("/files/{id}/download", name="download", defaults={"version" = null})
     * @Route("/files/{id}/download/{version}", name="download_version")
     * @Method("GET")
     *
     */
	public function downloadAction(Request $request, $id, $version)
	{
        $doctrine = $this->getDoctrine();
        $fileRepository = $doctrine->getRepository(File::class);
        $file = $fileRepository->find($id);
        if (null === $file):
            return new Response("document inexistant", Response::HTTP_NOT_FOUND);
        endif;
		if(null === $version):
			$version = $file->getLastVersion();
		else:
			$versionRepository = $doctrine->getRepository(Version::class);
			$version = $versionRepository->find($version);
		endif;
		
		if (null === $version):
			return new Response("fichier inexistant", Response::HTTP_NOT_FOUND);
		endif;
		$content = file_get_contents($version->getPath());
		$response = new Response();

		//set headers
		$response->headers->set('Content-Type', 'mime/type');
		$response->headers->set('Content-Disposition', 'attachment;filename="' . $file->getTitle() . '.' . $version->getTypeMime()->getMime());
		$response->setContent($content);
		return $response;
	}
	
    /**
     * Liste des fichiers
     *
     * @Route("/files/{begin}/{length}")
     * @Route("/files", name="listFiles", defaults={"length" = 10, "begin" = 0})
     * @Method("GET")
     *
     */
    public function showFiles(Request $request, $token)
    {
        $repository = $this->getDoctrine()->getRepository(File::class);

        if($request->query->count()):
            $files = $repository->getFilteredFiles($request->query->all());
        else:
            $files = $repository->findAll();
        endif;

        if ($files === null) {
            return new Response("files not found", Response::HTTP_NOT_FOUND);
        }

        $listFiles = [];

        foreach($files as $key => $file):
            $listFiles[$key] = array(
                'id' => $file->getId(),
                'title' => $file->getTitle(),
                'description' => $file->getDescription(),
				'has_pin' => $file->hasPin(),
                'show' => $this->generateUrl(
                    'showFile',
                    array('token' => $token, 'id' => $file->getId()), UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'last_file' => $this->buildLastFile($file, $token)
            );

        endforeach;

        $response = new Response();
        $response->setContent(json_encode($listFiles));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Afficher le fichier {id}
     *
     * @Route("/files/{id}", name="showFile", requirements={"id" = "\d+"})
     * @Method("GET")
     */
    public function showFile($id, $token)
    {
        $doctrine = $this->getDoctrine();
        $fileRepository = $doctrine->getRepository(File::class);
        $file = $fileRepository->find($id);
        if ($file === null) {
            return new Response("document inexistant", Response::HTTP_NOT_FOUND);
        }

        $file = array(
            'id' => $file->getId(),
            'title' => $file->getTitle(),
            'description' => $file->getDescription(),
            'has_pin' => $file->hasPin(),
            'last_file' => $this->buildLastFile($file, $token)
        );

        $response = new Response();
        $response->setContent(json_encode($file));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function buildLastFile($file, $token) {
        if($lastVersion = $file->getLastVersion()):

            $user = $lastVersion->getUser();

            $lastVersion =  [
                'version' => $lastVersion->getVersion(),
                'path' => $this->generateUrl(
                    'download',
                    array('token' => $token, 'id' => $file->getId()), UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'deposit_date' => $lastVersion->getDepositDate(),
                'user' =>  [
                    'id' => $user->getId(),
                    'sso_id' => $user->getSsoId(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'show' => $this->generateUrl(
                        'showUser',
                        array('token' => $token, 'id' => $user->getId()), UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ],
                'mime' => $lastVersion->getTypeMime()->getMime(),
                'type' => $lastVersion->getTypeMime() && $lastVersion->getTypeMime()->getTypeDocument()?
                    $lastVersion->getTypeMime()->getTypeDocument()->getLabel():null
            ];
        endif;
        return $lastVersion;
    }

    /**
     * Liste des utilisateurs
     *
     * @Route("/users", name="listUsers")
     * @Method("GET")
     */
    public function showUsers($token)
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();
        if ($users === null) {
            return new Response("users not found", Response::HTTP_NOT_FOUND);
        }
        $array = [];
        foreach($users as $key => $user):
            $array[$key] = array(
                'id' => $user->getId(),
                'sso_id' => $user->getSsoId(),
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
                'version' => $this->generateUrl('showUser',
                    array('token' => $token, 'id' => $user->getId()), UrlGeneratorInterface::ABSOLUTE_URL
                )
            );
        endforeach;

        $response = new Response();
        $response->setContent(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Liste des types de document
     *
     * @Route("/files/types", name="showTypes")
     * @Method("GET")
     */
    public function showTypes()
    {
        $repository = $this->getDoctrine()->getRepository(TypeDocument::class);
        $types = $repository->findAll();
        if ($types === null) {
            return new Response("types not found", Response::HTTP_NOT_FOUND);
        }
        $array = [];
        foreach($types as $key => $type):
            $array[$key] = array(
                'id' => $type->getId(),
                'type' => $type->getLabel(),
            );
        endforeach;
        $response = new Response();
        $response->setContent(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Ajout d'un type de fichier
     *
     * @Route("/files/types", name="addType")
     * @Method("POST")
     */
    public function addType(Request $request)
    {

        try {
            $doctrine = $this->getDoctrine();
            $manager = $doctrine->getManager();
            $data = $request->request;

            // génération de notre nouveau type
            $file = new TypeDocument();
            $file
                ->setLabel($data->get("label"));
            $manager->persist($file);
            $manager->flush();

        } catch (Exception $e) {
            $status = array('reponse' => 'pas ok');
        }

        $response = new Response();
        $response->setContent(json_encode($status));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Liste des fichiers d'un utilisateur
     *
     * @Route("/users/{id}", name="showUser", requirements={"id" = "\d+"})
     * @Method("GET")
     */
    public function showUser(Request $request, $token, $id)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $repository = $manager->getRepository(User::class);
        $user = $repository->find($id);
        if ($user === null) {
            return new Response("users not found", Response::HTTP_NOT_FOUND);
        }
        $array = [
            'id' => $user->getId(),
            'sso_id' => $user->getSsoId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'show' => $this->generateUrl(
                'showUser',
                array('token' => $token, 'id' => $user->getId()), UrlGeneratorInterface::ABSOLUTE_URL
            )
        ];
        $versions = $user->getVersions();
        foreach($versions as $key => $version):
            foreach($user->getVersions() as $version):
                $array['files'] = [
                    'id' => $version->getId(),
                    'path' => $version->getPath(),
                    'deposit_date' => $version->getDepositDate(),
                    'show' => $this->generateUrl('showFile',
                        array('token' => $token, 'id' => $version->getId()), UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ];
            endforeach;
        endforeach;

        $response = new Response();
        $response->setContent(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * test mime
     *
     * @Route("/mime/{extension}", name="testMime")
     * @Method("GET")
     */
    public function testMime(Request $request, $extension)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $typeMimeRepository = $manager->getRepository(TypeMime::class);
        $typeMime = $typeMimeRepository->findOneBy(array('mime' => $extension));

        $response = new Response();
		if($typeMime):
			$response->setContent(json_encode(array('id' => $typeMime->getid())));
		else:
			$response->setContent(json_encode(array('ret' => false)));
		endif;
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Afficher le fichier {id}
     *
     * @Route("/files/{id}/versions", name="showVersions", requirements={"id" = "\d+"})
     * @Method("GET")
     */
    public function showVersions($id, $token)
    {
        $doctrine = $this->getDoctrine();
        $fileRepository = $doctrine->getRepository(File::class);
        $file = $fileRepository->find($id);
        if ($file === null) {
            return new Response("document inexistant", Response::HTTP_NOT_FOUND);
        }

        $versions = $file->getVersions();

        $array = array(
            'title' => $file->getTitle(),
            'description' => $file->getDescription(),
            'pin' => $file->getPin()
        );

        foreach($versions as $version):
            $array['versions'][] = [
                'version' => $version->getVersion(),
                'path' => $this->generateUrl(
					'download',
					array(
						'token' => $token, 
						'id' => $file->getId(),
						'version' => $version->getId()
					), UrlGeneratorInterface::ABSOLUTE_URL
				),
                'deposit_date' => $version->getDepositDate(),
                'deposit_date' => $version->getSize()
            ];
        endforeach;

        $response = new Response();
        $response->setContent(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Ajout d'un fichier
     *
     * @Route("/files", name="addFile")
     * @Method("POST")
     */
    public function addFile(Request $request)
    {
        try {
            $doctrine = $this->getDoctrine();
            $manager = $doctrine->getManager();
            $data = $request->request;

            // génération de notre nouveau document
            $file = new File();
            $file
                ->setTitle($data->get("title"))
                ->setDescription($data->get("description"))
                ->setPin($data->get("pin"));
            $manager->persist($file);
            $manager->flush();
				
			// ajout des mots clef
			if($data->has('keywords')):
				foreach($data->get('keywords') as $element):
					$keyword = new Keyword();
					$manager->persist($keyword);
					$keyword
						->setKeyword($element)
						->addFile($file);
				endforeach;
			endif;

            // récupération du fichier uploadé
            $upload = $request->files->get('file');
            $status = array('status' => "success", "fileUploaded" => false);

            // Le fichier uploadé existe bien
            if(!is_null($upload)):

				// récupération de l'utilisateur actif
				$token = $request->get('token');
				$user = $this->loadUser($token);
				
				$version = $this->uploadFile($file, $user, $upload);
				
				$status = array(
					'status' => "success",
					"fileUploaded" => true,
					"file" => $version->getPath(),
					"id" => $file->getId()
				);
            endif;

        } catch (Exception $e) {
            $status = array('reponse' => 'pas ok');
        }

        $response = new Response();
        $response->setContent(json_encode($status));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * mise à jour du fichier {id}
     *
     * @Route("/files/{id}", name="updateFile", requirements={"id" = "\d+"})
     * @Method("PUT")
     */
    public function updateFile(Request $request, $id)
    {
        try {
            $doctrine = $this->getDoctrine();
            $data = $request->request;

            $fileRepository = $doctrine->getRepository(File::class);

            // récupération de notre document
            $file = $fileRepository->find($id);
            $file
                ->setTitle($data->get("title"))
                ->setDescription($data->get("description"))
                ->setPin($data->get("pin"));
				
            // récupération du fichier uploadé
            $upload = $request->files->get('file');
            $status = array('status' => "success","fileUploaded" => false);

            // Le fichier uploadé existe bien
            if(!is_null($upload)):

                // récupération de l'utilisateur actif
                $token = $request->get('token');
                $user = $this->loadUser($token);
			
				$version = $this->uploadFile($file, $user, $upload);

                $response = new Response();
                $response->setContent(json_encode($status));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            else:
                $response = new Response();
                $response->setContent(json_encode($status));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            endif;
        } catch (Exception $e) {
            $response = new Response();
            $response->setContent(json_encode(array('reponse' => 'pas ok')));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }
	
    /**
     * suppression du fichier {id}
     *
     * @Route("/files/{id}", name="deleteFile", requirements={"id" = "\d+"})
     * @Method("DELETE")
     */
    public function deleteFile($id)
    {
        $repository = $this->getDoctrine()->getRepository(File::class);
        $file = $repository->find($id);
        if ($file === null) {
            return new Response("fichier inexistant", Response::HTTP_NOT_FOUND);
        }
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($file);
        $manager->flush();
        $response = new Response();
        $response->setContent(json_encode(array('ret' => '1')));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function loadUser($token)
    {
        $headers = array('Accept' => 'application/json');

        $doctrine = $this->getDoctrine();
        $userRepository = $doctrine->getRepository(User::class);

        $api_response = Unirest\Request::get($this->getParameter('sso.url') . 'who_is/' . $token , $headers);
        if(!empty($api_response->body)):
            $user = $userRepository->findOneBy(array('ssoId' => $api_response->body->id));
            if(!$user):
                $user = new User();
                $user
                    ->setSsoId($api_response->body->id)
                    ->setFirstName($api_response->body->prenom)
                    ->setLastName($api_response->body->nom);
				$manager = $doctrine->getManager();
                $manager->persist($user);
                $manager->flush();
            endif;
        endif;
        return $user;
    }

}
