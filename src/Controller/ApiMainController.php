<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Unirest;


/**
 * @Route("/api")
 */
class ApiMainController extends Controller
{

    /**
     * @Route("/", name="api")
     */
    public function api()
    {
        return $this->render('api/doc.html.twig');
    }

}
