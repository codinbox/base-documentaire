<?php

namespace App\EventSubscriber;

use App\Controller\TokenAuthenticatedController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Unirest;

class TokenSubscriber implements EventSubscriberInterface
{
    private $ssoUrl;
    public function __construct($ssoUrl) {
        $this->ssoUrl = $ssoUrl;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {
            $token = $event->getRequest()->attributes->get('token');
            $headers = array('Accept' => 'application/json');
            $api_response = Unirest\Request::get($this->ssoUrl . 'check_access' . '/' . $token . '/docs', $headers);
            if($api_response->body->ret === "expired"):
                throw new AccessDeniedHttpException('This action needs a valid token!');
            endif;
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}
