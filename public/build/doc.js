/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/build/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/doc.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/css/doc.css":
/*!****************************!*\
  !*** ./assets/css/doc.css ***!
  \****************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./assets/js/doc.js":
/*!**************************!*\
  !*** ./assets/js/doc.js ***!
  \**************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../css/doc.css */ "./assets/css/doc.css");

$('header div[class*=-button-container').click(function () {
    if ($('nav>ul').hasClass('d-none')) {
        $('nav>ul').removeClass('d-none');
        $(this).fadeOut("fast", function () {
            $('.close-button-container').fadeIn("fast").css('display', 'flex');
        });
    } else {
        $('nav>ul').addClass('d-none');
        $(this).fadeOut("fast", function () {
            $('.burger-button-container').fadeIn("fast").css('display', 'flex');
        });
    }
    // $('nav ul').hasClass('d-md-none') ? $('nav ul').removeClass('d-md-none') : $('nav ul').addClass('d-md-none');
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNzQ4NzM2NjdmMDE0MWMxNTllN2IiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2Nzcy9kb2MuY3NzPzE0OGYiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2RvYy5qcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwiJCIsImNsaWNrIiwiaGFzQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImZhZGVPdXQiLCJmYWRlSW4iLCJjc3MiLCJhZGRDbGFzcyJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDN0RBLHlDOzs7Ozs7Ozs7Ozs7QUNBQSxtQkFBQUEsQ0FBUSw0Q0FBUjs7QUFFQUMsRUFBRSxxQ0FBRixFQUF5Q0MsS0FBekMsQ0FBK0MsWUFBVztBQUN0RCxRQUFJRCxFQUFFLFFBQUYsRUFBWUUsUUFBWixDQUFxQixRQUFyQixDQUFKLEVBQW9DO0FBQ2hDRixVQUFFLFFBQUYsRUFBWUcsV0FBWixDQUF3QixRQUF4QjtBQUNBSCxVQUFFLElBQUYsRUFBUUksT0FBUixDQUFnQixNQUFoQixFQUF3QixZQUFXO0FBQy9CSixjQUFFLHlCQUFGLEVBQTZCSyxNQUE3QixDQUFvQyxNQUFwQyxFQUE0Q0MsR0FBNUMsQ0FBZ0QsU0FBaEQsRUFBMkQsTUFBM0Q7QUFDSCxTQUZEO0FBR0gsS0FMRCxNQUtNO0FBQ0ZOLFVBQUUsUUFBRixFQUFZTyxRQUFaLENBQXFCLFFBQXJCO0FBQ0FQLFVBQUUsSUFBRixFQUFRSSxPQUFSLENBQWdCLE1BQWhCLEVBQXdCLFlBQVc7QUFDL0JKLGNBQUUsMEJBQUYsRUFBOEJLLE1BQTlCLENBQXFDLE1BQXJDLEVBQTZDQyxHQUE3QyxDQUFpRCxTQUFqRCxFQUE0RCxNQUE1RDtBQUNILFNBRkQ7QUFHSDtBQUNEO0FBQ0gsQ0FiRCxFIiwiZmlsZSI6ImRvYy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9idWlsZC9cIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vYXNzZXRzL2pzL2RvYy5qc1wiKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA3NDg3MzY2N2YwMTQxYzE1OWU3YiIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9hc3NldHMvY3NzL2RvYy5jc3Ncbi8vIG1vZHVsZSBpZCA9IC4vYXNzZXRzL2Nzcy9kb2MuY3NzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInJlcXVpcmUoJy4uL2Nzcy9kb2MuY3NzJyk7XG5cbiQoJ2hlYWRlciBkaXZbY2xhc3MqPS1idXR0b24tY29udGFpbmVyJykuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgaWYgKCQoJ25hdj51bCcpLmhhc0NsYXNzKCdkLW5vbmUnKSkge1xuICAgICAgICAkKCduYXY+dWwnKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgICAgICQodGhpcykuZmFkZU91dChcImZhc3RcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkKCcuY2xvc2UtYnV0dG9uLWNvbnRhaW5lcicpLmZhZGVJbihcImZhc3RcIikuY3NzKCdkaXNwbGF5JywgJ2ZsZXgnKTtcbiAgICAgICAgfSk7XG4gICAgfWVsc2Uge1xuICAgICAgICAkKCduYXY+dWwnKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgICAgICQodGhpcykuZmFkZU91dChcImZhc3RcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkKCcuYnVyZ2VyLWJ1dHRvbi1jb250YWluZXInKS5mYWRlSW4oXCJmYXN0XCIpLmNzcygnZGlzcGxheScsICdmbGV4Jyk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICAvLyAkKCduYXYgdWwnKS5oYXNDbGFzcygnZC1tZC1ub25lJykgPyAkKCduYXYgdWwnKS5yZW1vdmVDbGFzcygnZC1tZC1ub25lJykgOiAkKCduYXYgdWwnKS5hZGRDbGFzcygnZC1tZC1ub25lJyk7XG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9hc3NldHMvanMvZG9jLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==