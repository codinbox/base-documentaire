require('../css/doc.css');

$('header div[class*=-button-container').click(function() {
    if ($('nav>ul').hasClass('d-none')) {
        $('nav>ul').removeClass('d-none');
        $(this).fadeOut("fast", function() {
            $('.close-button-container').fadeIn("fast").css('display', 'flex');
        });
    }else {
        $('nav>ul').addClass('d-none');
        $(this).fadeOut("fast", function() {
            $('.burger-button-container').fadeIn("fast").css('display', 'flex');
        });
    }
    // $('nav ul').hasClass('d-md-none') ? $('nav ul').removeClass('d-md-none') : $('nav ul').addClass('d-md-none');
});